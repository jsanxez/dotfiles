" Display hidden characters
" set list
" :set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
" :set listchars=tab:→\ ,space:·,nbsp:␣,trail:•

" set font
set guifont=Gohu\ GohuFont\ Regular\ 10
" set colorscheme
" colorscheme peachpuff
" colorscheme slate
colorscheme pablo

" set the clipboard as the default register
set clipboard=unnamed
" set tab key
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" set wrap
set textwidth=79

" Status bar
set laststatus=2

" Display Options
set showmode
set showcmd

" Show line numbers
set number
" Relative numbers
set relativenumber

" Set status line display
set statusline=
set statusline+=%#WildMenu#
set statusline+=\ %F%m%h%w\ ‡
set statusline+=%r\ 
set statusline+=%#ToolbarButton#
set statusline+=\ [%l,%c]\%=--%p%%--\ 
set statusline+=%#PmenuSbar#
set statusline+=[Buffer:%n]

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Set incremental search
set incsearch

" Set case sensitive
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

" Setting vertical indicator line
set colorcolumn=+1
" highlight clear
" highlight ColorColumn ctermbg=darkgrey # only work with clear highlights.

" Setting Visual selection color
highlight Visual ctermbg=DarkRed ctermfg=Black

" Problemas de colores con tmux
set background=dark
set t_Co=256

"---------------------------"

"Vim plugins manager Plugged"
call plug#begin('~/.vim/plugged')
    Plug 'xuhdev/vim-latex-live-preview'
    Plug 'scrooloose/nerdtree'
    Plug 'turbio/bracey.vim'
    Plug 'mattn/emmet-vim'
call plug#end()

"
" Configuring pdf viewer for latex-previewer
let g:livepreview_previewer='zathura'
let g:livepreview_cursorhold_recompile=0
" Configuring emmet-vim
let g:user_emmet_leader_key='<C-Y>'
" Configuring Bracey
let g:bracey_refresh_on_save='1'
"----------------------------"


